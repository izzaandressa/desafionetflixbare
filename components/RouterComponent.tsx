import React, { useState } from "react";
import { Router, Scene } from "react-native-router-flux";
import styled from "styled-components/native";

import { MenuRoute } from "../models/MenuRoute.model";

// const Menu = styled.Text`
//   font-size: 18px;
//   color: #fff;
//   letter-spacing: 0.1px;
// `;

const RouterComponent: React.FC<MenuRoute> = (props) => {
  const [menu, setMenu] = React.useState(props.itens);

  return (
    <Router>
      <Scene key="root">
        {menu.map((item: string, key: number) => {
          <Scene
            key={item}
            component={item}
            title="Inicial"
            initial={key === 0}
          />;
        })}
      </Scene>
    </Router>
  );
};

export default RouterComponent;
