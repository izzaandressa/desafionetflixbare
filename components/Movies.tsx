import React, { useState, useEffect } from "react";
import { Dimensions } from "react-native";
import styled from "styled-components/native";

import { Movie } from "../models/Movie.model";
import { GetFilmsDiscover } from "../services/apiFilms";

const Container = styled.View`
  padding: 20px 0;
`;

const Label = styled.Text`
  color: #fff;
  font-size: 16px;
  margin: 0 0 5px 10px;
`;
const MovieScroll = styled.ScrollView`
  padding-left: 10px;
`;

const MoviePoster = styled.Image`
  width: ${Math.round((Dimensions.get("window").width * 28) / 100)}px;
  height: 150px;
`;

const MovieCard = styled.View`
  padding-right: 9px;
`;

const Movies: React.FC<Movie> = ({ label }) => {
  const [movies, setMovies] = useState([]);

  const init = async () => {
    const math = Math.floor(Math.random() * (50 - 1)) + 1 + 1;

    const response = await GetFilmsDiscover(math).catch((err) =>
      console.error("")
    );

    if (response) {
      setMovies(response.data.results);
    }
  };

  useEffect(() => {
    init();
  }, [label]);

  return (
    <Container>
      <Label>{label}</Label>
      <MovieScroll horizontal>
        {movies.map((movie: any) => {
          const uri = `https://image.tmdb.org/t/p/w342/${movie.poster_path}`;

          return (
            <MovieCard key={String(movie.poster_path)}>
              <MoviePoster resizeMode="cover" source={{ uri }} />
            </MovieCard>
          );
        })}
      </MovieScroll>
    </Container>
  );
};

export default Movies;
