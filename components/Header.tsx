import { NavigationContext } from "@react-navigation/native";
import React, { useContext, useState } from "react";
import { TouchableOpacity, Text } from "react-native";
import styled from "styled-components/native";

import TranslateContext from "../contexts/Translate";

const Container = styled.View`
  align-items: center;
  flex-direction: row;
  justify-content: space-between;
  padding: 25px 25px 0 25px;
  width: 100%;
`;

const Logo = styled.Image`
  width: 20px;
  height: 40px;
`;

const Menu = styled.Text`
  font-size: 18px;
  color: #fff;
  letter-spacing: 0.1px;
`;

const Header: React.FC = () => {
  const { t } = useContext(TranslateContext);
  const { navigate } = useContext(NavigationContext);

  const [MenuContent] = useState([t("SERIES"), t("FILMS"), t("MY_LIST")]);

  const handleClick = (page) => {
    navigate(page, { params: { paramName: page } });
  };

  return (
    <Container>
      <Logo resizeMode="contain" source={require("../assets/img/logo.png")} />

      {MenuContent.map((item) => (
        <TouchableOpacity onPress={() => handleClick(item)}>
          <Text>
            <Menu>{item}</Menu>
          </Text>
        </TouchableOpacity>
      ))}
    </Container>
  );
};

export default Header;
