import * as Localization from "expo-localization";

import axios from "./api";

const apiKey = "api_key=ea3fce007ded72bfc1ad864bf245b778";
const language = `language=${Localization.locale}`;

export const GetFilmsDiscover = (page: any) => {
  const url = `discover/movie?${apiKey}&${language}&page : any=${page}`;
  return axios.get(url);
};

export const GetFilmsTrending = (page: any) => {
  const url = `trending/movie/week?${apiKey}&${language}&page=${page}`;
  return axios.get(url);
};

export const GetGenders = () => {
  const url = `genre/movie/list?${apiKey}&${language}`;
  return axios.get(url);
};

export const GetFilm = (id: number) => {
  const url = `movie/${id}`;
  return axios.get(url);
};
