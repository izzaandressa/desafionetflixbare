import axios from "axios";

import { BASE_URL } from "../utils/Contantes";

const api = axios.create({
  baseURL: BASE_URL,
});

export default api;
