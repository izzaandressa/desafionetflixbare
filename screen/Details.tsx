import { NavigationContext } from "@react-navigation/core";
import { LinearGradient } from "expo-linear-gradient";
import React, { useContext, useState, useEffect } from "react";
import { StatusBar, Dimensions } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import styled from "styled-components/native";

const Container = styled.ScrollView`
  flex: 1;
  background-color: #000;
`;

const Poster = styled.ImageBackground`
  width: ${Math.round((Dimensions.get("window").width * 100) / 100)}px;
  height: ${(Dimensions.get("window").height * 81) / 100}px;
`;

const Gradient = styled(LinearGradient)`
  height: 100%;
`;

const MovieCard = styled.View`
  padding-right: 9px;
`;

const MovieScroll = styled.ScrollView`
  padding-left: 10px;
`;

export const ContainerParent = styled.SafeAreaView`
  flex: 1;
  background-color: black;
`;

export const ContainerStart = styled.View`
  width: 100%;
  height: 60px;
  flex-direction: row;
`;

export const ContainerImage = styled.Image`
  width: 100%;
  height: 250px;
`;

export const ContainerAvailable = styled.View`
  flex: 1;
  justify-content: flex-end;
  align-items: center;
  right: 15px;
  flex-direction: row;
`;

export const ContainerRelease = styled.View`
  flex: 1;
  justify-content: flex-start;
  align-items: center;
  left: 15px;
  flex-direction: row;
`;

export const TextDetails = styled.Text`
  color: white;
  font-size: "16px";
  font-weight: normal;
`;

export const ContainerDetail = styled.View`
  padding: 15px;
`;

export const Ikon = styled(Icon).attrs({
  size: 20,
})`
  color: white;
  margin-left: 5px;
`;

const Details: React.FC = ({ route, navigation }) => {
  const { film } = route.params;

  const uri = `https://image.tmdb.org/t/p/w342/${film.backdrop_path}`;

  return (
    <ContainerParent>
      <Container>
        <ContainerImage source={{ uri }} />
        <ContainerStart>
          <ContainerRelease>
            <TextDetails>{`Lançamento:  `}</TextDetails>
          </ContainerRelease>
          <ContainerAvailable>
            <TextDetails>{`Avaliação: `}</TextDetails>
            <TextDetails>{`${film.backdrop_path.vote_average}`}</TextDetails>
            <Ikon name="ios-star" color="orange" />
          </ContainerAvailable>
        </ContainerStart>
        <ContainerDetail>
          <TextDetails style={{ marginBottom: 10 }}>filmResumo</TextDetails>
          <TextDetails>{film.backdrop_path.overview}</TextDetails>
        </ContainerDetail>
      </Container>
    </ContainerParent>
  );
};

export default Details;
