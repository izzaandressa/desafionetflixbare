import { LinearGradient } from "expo-linear-gradient";
import React, { useContext, useState, useEffect } from "react";
import { StatusBar, Dimensions } from "react-native";
import styled from "styled-components/native";

import Header from "../components/Header";
import Hero from "../components/Hero";
import Movies from "../components/Movies";
import TranslateContext from "../contexts/Translate";
import { GetFilmsTrending } from "../services/apiFilms";

const Container = styled.ScrollView`
  flex: 1;
  background-color: #000;
`;

const Poster = styled.ImageBackground`
  width: ${Math.round((Dimensions.get("window").width * 100) / 100)}px;
  height: ${(Dimensions.get("window").height * 81) / 100}px;
`;

const Gradient = styled(LinearGradient)`
  height: 100%;
`;

const MovieCard = styled.View`
  padding-right: 9px;
`;

const MovieScroll = styled.ScrollView`
  padding-left: 10px;
`;

const Series: React.FC = () => {
  const { t } = useContext(TranslateContext);
  const [banner, setBanner] = useState([]);
  const init = async () => {
    const math = Math.floor(Math.random() * (50 - 1)) + 1 + 1;

    const response = await GetFilmsTrending(math).catch((err) =>
      console.error("")
    );

    if (response) {
      setBanner(response.data.results);
    }
  };

  useEffect(() => {
    init();
  }, []);

  return (
    <>
      <StatusBar
        translucent
        backgroundColor="transparent"
        barStyle="light-content"
      />
      <Container>
        <MovieScroll horizontal>
          {banner.map((movie: any, key) => {
            const uri = `https://image.tmdb.org/t/p/w342/${movie.poster_path}`;
            return (
              <MovieCard key={String(movie.poster_path)}>
                <Poster source={{ uri }}>
                  <Gradient
                    locations={[0, 0.2, 0.6, 0.93]}
                    colors={[
                      "rgba(0,0,0,0.5)",
                      "rgba(0,0,0,0.0)",
                      "rgba(0,0,0,0.0)",
                      "rgba(0,0,0,1)",
                    ]}
                  >
                    <Header />
                    <Hero />
                  </Gradient>
                </Poster>
              </MovieCard>
            );
          })}
        </MovieScroll>

        {[t("SURROUNDING"), t("EXCITING"), t("ACTION"), t("FICTION")].map(
          (category) => (
            <Movies label={category} />
          )
        )}
      </Container>
    </>
  );
};

export default Series;
