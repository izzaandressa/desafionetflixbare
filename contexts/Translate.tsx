import * as Localization from "expo-localization";
import i18n from "i18n-js";
import React, { createContext } from "react";

import * as PT from "../assets/i18n/pt.json";

i18n.translations = {
  "pt-BR": PT,
  "en-US": PT,
};

i18n.locale = Localization.locale;
i18n.fallbacks = true;

const TranslateContext: React.Context<any> = createContext(i18n);

export const TranslateProvider: React.FC = ({ children }) => (
  <TranslateContext.Provider value={i18n}>{children}</TranslateContext.Provider>
);

export default TranslateContext;
