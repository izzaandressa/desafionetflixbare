import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import React, { useContext } from "react";

import TranslateContext, { TranslateProvider } from "./contexts/Translate";
import Details from "./screen/Details";
import Films from "./screen/Films";
import Home from "./screen/Home";
import Series from "./screen/Series";

const Stack = createStackNavigator();

const App: React.FC = () => {
  const { t } = useContext(TranslateContext);

  return (
    <TranslateProvider>
      <NavigationContainer>
        <Stack.Navigator screenOptions={{ headerShown: false }}>
          <Stack.Screen name={t("SERIES")} component={Series} />
          <Stack.Screen name={t("FILMS")} component={Films} />
          <Stack.Screen name={t("MY_LIST")} component={Home} />
        </Stack.Navigator>
      </NavigationContainer>
    </TranslateProvider>
  );
};

export default App;
