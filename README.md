
## O que a plataforma é capaz de fazer :checkered_flag:
- [X] "Banner" com os próximos filmes a lançar (/trending). :heavy_check_mark:
- [X] Lista ou seções com os filmes mais populares (/discover) :heavy_check_mark:
- [X] Localize a aplicação para o idioma do telefone do usuário .:heavy_check_mark:
- [X] Fazer uma home para séries, com as mesmas funcionalidades. :heavy_check_mark:

- [X] Utiliza o expo; :heavy_check_mark:
- [X] Hooks; :heavy_check_mark:
- [X] TypeScript; :heavy_check_mark:
- [X] I18n; :heavy_check_mark:
- [X] Eslint & Prettier; :heavy_check_mark:

###  Preparar o ambiente 
http://react-native.rocketseat.dev/

# instalar dependências
npm install    

# ou  
yarn install

###  Rodar o app
yarn android 
yarn start

